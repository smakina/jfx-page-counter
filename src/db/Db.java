package db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import config.*;

public class Db {
    public static Connection connection;
    private DbHelper dbHelper;

    public Db(){
        File file = new File(config.DB_PATH);

        if (!file.exists()){
            file.mkdirs();
        }

        getConnection();
    }

    private void getConnection(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:"+config.DB_PATH+config.DB_NAME);
            dbHelper = new DbHelper();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public DbHelper getDbHelper() {
        return dbHelper;
    }
}