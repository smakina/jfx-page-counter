package db;

import model.CorruptFile;
import model.Count;
import sun.rmi.runtime.Log;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DbHelper {
    private String tableName = "counts";

    public DbHelper() {
        createTables();
    }

    public boolean insertCount(Count count) {
        String query = "INSERT INTO counts(" +
                "totalPages, totalFiles, totalCorrupt, path)" +
                "VALUES('" +
                count.getTotalPages().replace(",", "") + "','" +
                count.getTotalFiles().replace(",", "") + "','" +
                count.getTotalCorrupt().replace(",", "") + "','" +
                count.getPath() + "'" +
                ")";

        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }


    private void createTables() {

        String query =
                "CREATE TABLE IF NOT EXISTS counts(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "totalPages INTEGER," +
                        "totalFiles INTEGER," +
                        "totalCorrupt INTEGER," +
                        "timeStamp DATETIME DEFAULT CURRENT_TIMESTAMP," +
                        "path TEXT)";
        executeQuery(query);

        query =
                "CREATE TABLE IF NOT EXISTS corrupt_files(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "count_id INTEGER," +
                        "message TEXT," +
                        "path TEXT," +
                        "timeStamp DATETIME DEFAULT CURRENT_TIMESTAMP)";

        executeQuery(query);
    }

    private void executeQuery(String query) {
        PreparedStatement pr;
        try {
            pr = Db.connection.prepareStatement(query);
            pr.execute();

            System.out.println("Table Created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteColumn(int id) {
        String query = "DELETE FROM counts WHERE id='" + id + "'";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            System.out.println("record deleted " + id);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public int getLastInsertedId(String table){
        String query = "SELECT id FROM "+table+" ORDER BY id DESC LIMIT 1";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("id");
            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public ArrayList<Count> getCounts() {
        ArrayList<Count> counts = new ArrayList<>();

        String query = "SELECT * FROM counts ORDER BY id DESC";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            while (resultSet.next()) {

                counts.add(new Count(
                        resultSet.getInt("id"),
                        resultSet.getInt("totalPages"),
                        resultSet.getInt("totalFiles"),
                        resultSet.getInt("totalCorrupt"),
                        resultSet.getString("timeStamp"),
                        resultSet.getString("path")
                ));

            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return counts;
    }

    public boolean saveCorruptFiles(int countId, CorruptFile corruptFile) {
        String query = "INSERT INTO corrupt_files(" +
                "count_id, message, path)" +
                "VALUES('"+countId+"', '"+corruptFile.getMessage()+"', '"+corruptFile.getFile().getAbsolutePath()+"')";

        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<CorruptFile> getCorruptFiles(int id) {
        List<CorruptFile> corruptFileList = new ArrayList<>();

        String query = "SELECT * FROM corrupt_files WHERE count_id = "+id+" ORDER BY id DESC";
        System.out.println(query);
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            while (resultSet.next()) {
                File file = new File(resultSet.getString("path"));
                corruptFileList.add(new CorruptFile(file, ""+resultSet.getString("message") ));
            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return corruptFileList;
    }
}
