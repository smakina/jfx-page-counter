package helper;

import db.DbHelper;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.CorruptFile;
import model.Count;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


public class CorruptFileListHandler implements Initializable {

    @FXML
    TextField filterField;

    @FXML
    private TableView<CorruptFile> tableView;
    private List<CorruptFile> corruptFileList;
    private DbHelper dbHelper;
    private Count count;
    private ObservableList<CorruptFile> masterData = FXCollections.observableArrayList();
    SortedList<CorruptFile> sortedData;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dbHelper = new DbHelper();

        setColumns();
    }

    public void setData(Count count){
        this.count= count;
        masterData.addAll(getData());

        FilteredList<CorruptFile> filteredData = new FilteredList<>(masterData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(myObject -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name field in your object with filter.
                String lowerCaseFilter = newValue.toLowerCase();

                if (String.valueOf(myObject.getPath()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                    // Filter matches first name.

                } else if (String.valueOf(myObject.getMessage()).toLowerCase().contains(lowerCaseFilter)) {
                    return false; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<CorruptFile> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        tableView.setItems(sortedData);

    }

    public static void addTextFilter(ObservableList<List<Object>> allData,
                                     TextField filterField, TableView<List<Object>> table) {

        final List<TableColumn<List<Object>, ?>> columns = table.getColumns();

        FilteredList<List<Object>> filteredData = new FilteredList<>(allData);
        filteredData.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            String text = filterField.getText();

            if (text == null || text.isEmpty()) {
                return null;
            }
            final String filterText = text.toLowerCase();

            return o -> {
                for (Object value : columns) {
                    if (value != null && value.toString().toLowerCase().equals(filterText)) {
                        return true;
                    }
                }
                return false;
            };
        }, filterField.textProperty()));

        SortedList<List<Object>> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortedData);
    }


    private void setColumns(){
        // Add columns to the TableView
        tableView.getColumns().addAll(
                getTH("Message", "message"),
                getTH("File Location", "path"),
                getTH("", "controls")
        );

        // Set the column resize policy to constrained resize policy
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty table
        tableView.setPlaceholder(new Label(""));
        tableView.setId("table");
    }

    public static TableColumn<CorruptFile, Integer> getTH(String name, String ref){
        TableColumn<CorruptFile, Integer> idCol = new TableColumn<>(name);
        PropertyValueFactory<CorruptFile, Integer> idCellValueFactory = new PropertyValueFactory<>(ref);
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    private List<CorruptFile> getData(){
        return dbHelper.getCorruptFiles(count.getId());
    }

    private void loadTableData(){
        tableView.getItems().clear();
        tableView.setItems(sortedData);
        tableView.refresh();
    }


}
