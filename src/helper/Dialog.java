package helper;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import model.CorruptFile;
import model.Count;

import java.io.IOException;

public class Dialog {
    public static void dialogList(ActionEvent event, Count count) {
        Stage stage = new Stage();
        Parent root = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("../main/dialog-corrupt-file.fxml"));
            root = fxmlLoader.load();

//            root = FXMLLoader.load(
//                    Main.class.getResource("../main/dialog-corrupt-file.fxml"));

            CorruptFileListHandler controller = fxmlLoader.<CorruptFileListHandler>getController();
            controller.setData(count);

            stage.setScene(new Scene(root));
            stage.setTitle("Select Gadgets");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(
                    ((Node)event.getSource()).getScene().getWindow() );
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
//            System.out.println(e.getMessage());
        }
    }
}
