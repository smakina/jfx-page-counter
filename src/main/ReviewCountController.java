package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import model.Count;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static main.Helper.formatNumber;

public class ReviewCountController implements Initializable {
    @FXML
    private PieChart pieChart;
    @FXML
    private Label corruptLabel;
    @FXML
    private Label unCorruptLabel;
    @FXML
    private Label totalPagesLabel;

    public static Count count;
    private int totalFiles = 0;
    private int totalCorrupt = 0;
    private int totalPages = 0;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        totalFiles = Integer.parseInt(count.getTotalFiles().replace(",", ""));
        totalCorrupt = Integer.parseInt(count.getTotalCorrupt().replace(",", ""));
        totalPages = Integer.parseInt(count.getTotalPages().replace(",", ""));

        corruptLabel.setText(formatNumber(totalCorrupt));
        unCorruptLabel.setText(formatNumber(totalFiles));
        totalPagesLabel.setText(formatNumber(totalPages));

        setPieChat();
    }

//    private void setDateView() {
//        DatePickerSkin datePickerSkin = new DatePickerSkin(new DatePicker(LocalDate.now()));
//        Node popupContent = datePickerSkin.getPopupContent();
//        AnchorPane.setTopAnchor(popupContent, 0.0);
//        AnchorPane.setRightAnchor(popupContent, 0.0);
//        AnchorPane.setBottomAnchor(popupContent, 0.0);
//        AnchorPane.setLeftAnchor(popupContent, 0.0);
//
////        dateView.getChildren().add(popupContent);
//    }

    private void setPieChat(){
        //Preparing ObservbleList object
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                new PieChart.Data("Corrupt files", totalCorrupt),
                new PieChart.Data("Uncorrupted", totalFiles));

        pieChart.setData(pieChartData);
        //Setting the title of the Pie chart
        pieChart.setTitle("FIle summary");
        //setting the direction to arrange the data
        pieChart.setClockwise(true);
    }
}
