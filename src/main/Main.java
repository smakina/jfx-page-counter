package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    public static Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        mainStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/main/main-ui.fxml"));
        mainStage.setTitle("PageCounter");
        Scene scene = new Scene(root, 900, 650);
        scene.getStylesheets().add(Main.class.getResource("/res/css/style.css").toExternalForm());
        mainStage.getIcons().add(new Image(Main.class.getResourceAsStream("/res/img/pdfIcon.png")));
        mainStage.setScene(scene);
        mainStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
