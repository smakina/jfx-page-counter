package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Count;

public class TableViewHelper {
    // Returns an observable list of persons
    public static ObservableList<Count> getCountList()    {
        Count p1 = new Count(1,100, 100, 200, "04/04/2020", "C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");
        Count p2 = new Count(1, 200,100, 200, "04/04/2020", "C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");
        Count p3 = new Count(1, 400,100, 200, "04/04/2020", "C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");
        Count p4 = new Count(1, 590,100, 200, "04/04/2020","C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");
        Count p5 = new Count(1, 540,100, 200, "04/04/2020", "C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");
        Count p6 = new Count(1, 4900,100, 200, "04/04/2020", "C:\\Users\\SALVAT~1\\AppData\\Local\\Temp");

        return FXCollections.observableArrayList(p1, p2, p3, p4, p5, p6);
    }

    public static TableColumn<Count, Integer> getTFiles(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("Total Files");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("totalFiles");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }



    public static TableColumn<Count, Integer> getTPages(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("Total Pages");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("totalPages");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<Count, Integer> getTCorrupt(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("Total Corrupted");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("totalCorrupt");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<Count, Integer> getTimeStamp(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("Time Stamp");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("timeStamp");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<Count, Integer> getTPath(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("folder");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("folder");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<Count, Integer> getControls(){
        TableColumn<Count, Integer> idCol = new TableColumn<>("");
        PropertyValueFactory<Count, Integer> idCellValueFactory = new PropertyValueFactory<>("controls");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }
}
