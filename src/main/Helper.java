package main;


import com.itextpdf.text.pdf.PdfReader;
import com.jfoenix.controls.JFXButton;
import db.DbHelper;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.CorruptFile;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

public class Helper {
    public static List<CorruptFile> corruptFileList;
    public static String formatNumber(int number){
        return NumberFormat.getInstance().format(number);
    }

    public static int countPages(File file){
        PdfReader pdfReader;
        try {
            pdfReader = new PdfReader(file.getAbsolutePath());
        } catch (IOException e) {
            corruptFileList.add(new CorruptFile(file, e.getMessage()));
            System.out.println(e.getMessage());
            return 0;
        }

        return pdfReader.getNumberOfPages();
    }

    public static void customAlert(String msg, boolean isMessage) {
        AlertController.alertMsg = msg;
        AlertController.isMessage = isMessage;
        AlertController.alertStage = startStage("/main/alert.fxml", 411, 72);
    }

    public static Stage startStage(String path, int width, int height){
        Stage stage = new Stage();
        Parent root;

        try {
            root = FXMLLoader.load(Main.class.getResource(path));
        } catch (IOException e) {
            return null;
        }

        stage.setTitle("PageCounter");
        Scene scene = new Scene(root, width, height);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("/res/img/pdfIcon.png")));
//        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
        return stage;
    }

    public static void showAlert(Alert.AlertType type, String title, String message){
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }

    public static JFXButton createButton(String text, String color) {
        JFXButton button = new JFXButton(text);
//        Image image = new Image(getClass().getResource(path).toString(), true);
//        ImageView view = new ImageView(image);
//        view.setFitHeight(15);
//        view.setPreserveRatio(true);
//        button.setGraphic(view);
//        view.setSmooth(true);
        button.setStyle("-fx-background-color:"+color);
        button.setPadding(new Insets(2, 10, 2, 10));
        return button;
    }

    public static void openFileLocation(String path) throws IOException, InterruptedException {
        Process proc = Runtime.getRuntime().exec("explorer.exe /select, " + path.replaceAll("/", "\\\\"));
        proc.waitFor();
//        try {
//            Desktop.getDesktop().open(new File(path));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String completeCmd = "explorer.exe /select," + path;
//        new ProcessBuilder(("explorer.exe " + completeCmd).split(" ")).start();
    }

    public static void previewFile(String path){
        try {
            Desktop.getDesktop().open(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
