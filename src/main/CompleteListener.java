package main;

public interface CompleteListener {
    void onComplete();
    void onError(Exception e);
}
