package main;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AlertController implements Initializable {
    public static int id = -1;
    public static boolean isMessage = false;
    public static String alertMsg;
    public static Stage alertStage;


    @FXML
    private JFXButton deleteBtn;
    @FXML
    private Text message;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        message.setText(alertMsg);

        if (isMessage){
          deleteBtn.setVisible(false);
        }

        deleteBtn.setOnAction(event->{
            if (id != -1){
                if (TableController.dbHelper.deleteColumn(id)) {
                    Controller.tableController.fetchData();
                    alertStage.close();
                }
            }
        });
    }
}
