package main;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.DirectoryChooser;

import java.io.*;
import java.util.List;

import static main.Helper.formatNumber;
import static main.Helper.showAlert;

public class FileController {

    private String basePath;
    private String cachedPath;
    private String store = "path.conf";

    public FileController() {
        basePath = System.getProperty("java.io.tmpdir");
        setInitialDir();
    }

    private void setInitialDir() {
        try {
            cachedPath = getBasePath();
        } catch (IOException e) {
//            showAlert(AlertType.ERROR, "ERROR", e.getMessage());
        }
    }

    public File chooseDirectory() {
        setInitialDir();
        DirectoryChooser directoryChooser = new DirectoryChooser();

        if (cachedPath != null)
            directoryChooser.setInitialDirectory(new File(cachedPath));

        return directoryChooser.showDialog(Main.mainStage);
    }

    public void saveToFile(String content, String filename) {
        File file = new File(basePath + "\\" + filename);
        try {
            PrintWriter writer;
            writer = new PrintWriter(file);
            writer.println(content);
            writer.close();
        } catch (IOException e) {
//            showAlert(AlertType.ERROR, "ERROR", e.getMessage());
        }
    }

    public String getBasePath() throws IOException {
        File file = new File(basePath + "\\" + store);
        BufferedReader br = new BufferedReader(new FileReader(file));
        return br.lines().findFirst().orElse(null);
    }

    public void listf(String directoryName, List<File> files, SimpleStringProperty simpleStringProperty) {
        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] fList = directory.listFiles();

        if (fList != null)
            for (File file : fList) {
                if (file.isFile() && file.getName().toLowerCase().endsWith(".pdf")) {
                    files.add(file);
                    Platform.runLater(() -> {
                        simpleStringProperty.setValue(formatNumber(files.size()));
                    });
                } else if (file.isDirectory()) {
                    listf(file.getAbsolutePath(), files, simpleStringProperty);
                }
            }
    }

}


