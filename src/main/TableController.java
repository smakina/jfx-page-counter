package main;

import db.DbHelper;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import model.Count;
import db.Db;

import java.util.ArrayList;

public class TableController {
    public static TableView<Count> tableView;
    public static DbHelper dbHelper;
    private ArrayList<Count> countArrayList;

    public TableController(TableView<Count> tableView){
        TableController.tableView = tableView;
        setColumns();
        Db db = new Db();
        dbHelper = db.getDbHelper();
        countArrayList = new ArrayList<>();

        fetchData();
    }

    private void setColumns(){
        // Add columns to the TableView
        tableView.getColumns().addAll(
                TableViewHelper.getTFiles(),
                TableViewHelper.getTPages(),
                TableViewHelper.getTCorrupt(),
                TableViewHelper.getTimeStamp(),
                TableViewHelper.getTPath(),
                TableViewHelper.getControls()
        );

        // Set the column resize policy to constrained resize policy
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty table
        tableView.setPlaceholder(new Label(""));
        tableView.setId("countTable");
    }

    public void fetchData(){
        Runnable runnable = ()->{
            ArrayList<Count> counts = dbHelper.getCounts();
            Platform.runLater(()->{
                setCountArrayList(counts);
                loadTableData();
            });
        };
        new Thread(runnable).start();
    }

    public void setCountArrayList(ArrayList<Count> countArrayList) {
        this.countArrayList = countArrayList;
    }

    private void loadTableData(){
        tableView.getItems().clear();
        tableView.getItems().addAll(countArrayList);
        tableView.refresh();
    }

}
