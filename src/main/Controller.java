package main;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import model.CorruptFile;
import model.Count;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static main.Helper.*;

public class Controller implements Initializable, CompleteListener {

    @FXML
    private JFXButton chooseDir;
    @FXML
    private Label totalFiles;
    @FXML
    private Label totalPages;
    @FXML
    Label totalCorrupt;
    @FXML
    Label remainingLabel;
    @FXML
    Label status;

    @FXML
    private TableView<Count> tableView;

    private int TOTAL_FILES = 0;
    private int TOTAL_PAGES = 0;
    private int TOTAL_CORRUPT = 0;

    private String BASE_FOLDER = "";

    private List<File> fileList = new ArrayList<>();
    private FileController fileController;
    public static TableController tableController;

    private SimpleStringProperty simpleStringProperty = new SimpleStringProperty();
    private SimpleStringProperty countedPages = new SimpleStringProperty();
    private SimpleStringProperty corruptPages = new SimpleStringProperty();
    private SimpleStringProperty remainingFiles = new SimpleStringProperty();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fileController = new FileController();

        chooseDir.setOnAction(actionEvent -> {
            File file = fileController.chooseDirectory();

            if (file != null) {
                fileController.saveToFile(file.getAbsolutePath(), "path.conf");
                BASE_FOLDER = file.getAbsolutePath();
                setFileList(BASE_FOLDER);
            } else {
                System.out.println("Dir null");
            }
        });

        bind();
        setTableView();
    }

    private void setTableView() {
        tableController = new TableController(tableView);
    }

    private void setFileList(String path) {

        Runnable runnable = () -> {
            fileController.listf(path, fileList, simpleStringProperty);
            onComplete();
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void bind() {

        totalFiles.textProperty().bind(simpleStringProperty);
        totalPages.textProperty().bind(countedPages);
        totalCorrupt.textProperty().bind(corruptPages);
        remainingLabel.textProperty().bind(remainingFiles);

        simpleStringProperty.set("0");
        countedPages.set("0");
        corruptPages.set("0");
        remainingFiles.set("0");
        countedPages.set("0");
    }

    private void startCounting() {
        corruptFileList = new ArrayList<>();

        int totalP = 0;
        int totalC = 0;
        int processed = 0;
        TOTAL_FILES = fileList.size();

        for (File file : fileList) {
            int pages = countPages(file);

            if (pages == 0) {
                totalC += 1;
            }

            totalP += pages;


            int finalTotalP = totalP;
            int finalTotalC = totalC;
            processed++;

            int re = fileList.size() - processed;

            Platform.runLater(() -> {
                countedPages.set(formatNumber(finalTotalP));
                corruptPages.set(formatNumber(finalTotalC));
                remainingFiles.set(formatNumber(re));
            });
        }

        TOTAL_PAGES = totalP;
        TOTAL_CORRUPT = totalC;

        Platform.runLater(this::countingCompleted);

    }

    private void countingCompleted() {
        chooseDir.setDisable(false);
        fileList.clear();

        Count count = new Count(0, TOTAL_PAGES, TOTAL_FILES, TOTAL_CORRUPT, null, BASE_FOLDER);

        if (TOTAL_FILES == 0) {
            customAlert("Folder does not contain any PDF file ", true);
            return;
        }

        if (TableController.dbHelper.insertCount(count)) {
            saveCorruptFiles();
        }
    }

    private void saveCorruptFiles() {
        status.setText("Saving Data Please wait...!");
        Runnable runnable = () -> {
            int lastId = TableController.dbHelper.getLastInsertedId("counts");

            if (lastId == -1) return;
            for (CorruptFile corruptFile : corruptFileList){
                System.out.println("save corupt file");
                TableController.dbHelper.saveCorruptFiles(lastId, corruptFile);
            }
            completeSavingData();
        };
        runnable.run();
    }

    private void completeSavingData(){
        Platform.runLater(() -> {
            tableController.fetchData();
            status.setText("Data Saved!");
        });
    }

    @Override
    public void onComplete() {
        Platform.runLater(() -> {
            chooseDir.setDisable(true);
        });

        startCounting();
    }

    @Override
    public void onError(Exception e) {
        Platform.runLater(() -> {
            chooseDir.setDisable(true);
        });

    }
}
