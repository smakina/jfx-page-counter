package model;

import com.jfoenix.controls.JFXButton;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import main.*;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;

import static helper.Dialog.dialogList;
import static main.Helper.*;

public class Count {
    private int id;
    private HBox controls;
    private int totalPages;
    private int totalFiles;
    private int totalCorrupt;
    private String timeStamp;
    private String path;
    private String folder;

    public Count(int id, int totalPages, int totalFiles, int totalCorrupt, String timeStamp, String path) {
        this.id = id;
        this.totalPages = totalPages;
        this.totalFiles = totalFiles;
        this.totalCorrupt = totalCorrupt;
        this.timeStamp = timeStamp;
        this.path = path;

        setFolder();
    }

    private void setFolder() {
        File file = new File(path);
        folder = file.getName();
    }

    public int getId() {
        return id;
    }

    public String getTotalPages() {
        return formatNumber(totalPages);
    }

    public String getTotalFiles() {
        return formatNumber(totalFiles);
    }

    public String getTotalCorrupt() {
        return formatNumber(totalCorrupt);
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getPath() {
        return path;
    }

    public HBox getFolder() {
        HBox hBox = new HBox();
        Image image = new Image(getClass().getResource("/res/img/folder.png").toString(), true);
        ImageView view = new ImageView(image);
        view.setFitHeight(18);
        view.setPreserveRatio(true);
        view.setSmooth(true);

        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(5);
        Label label = new Label(folder);
        hBox.getChildren().addAll(view, label);
        return hBox;
    }

    public HBox getControls() {
        HBox hBox = new HBox();

        JFXButton delete = createButton("Delete", "#f44336");

        Label label = new Label(" | ");
        label.setPadding(new Insets(3, 0, 0, 0));

        JFXButton view = createButton("Review", "#1976d2");

        delete.setOnAction((event) -> {
            AlertController.id = getId();
            vonfirmDelete();
        });

        view.setOnAction(actionEvent -> {
            dialogList(actionEvent, Count.this);
        });

//        button.setGraphic( new MaterialDesignIconView(MaterialDesignIcon.LED_ON));
        hBox.getChildren().add(delete);
        hBox.getChildren().add(label);
        hBox.getChildren().add(view);

        hBox.setAlignment(Pos.CENTER);
//        hBox
        return hBox;
    }

    private JFXButton createButton(String text, String color) {
        JFXButton button = new JFXButton(text);
//        Image image = new Image(getClass().getResource(path).toString(), true);
//        ImageView view = new ImageView(image);
//        view.setFitHeight(15);
//        view.setPreserveRatio(true);
//        button.setGraphic(view);
//        view.setSmooth(true);
        button.setStyle("-fx-background-color: #eeeeee;");
        button.setPadding(new Insets(2, 10, 2, 10));
        return button;
    }

    private void vonfirmDelete() {
        customAlert("Are you sure you want to delete ?", false);
    }

    private void review() {

//        ReviewCountController.count = this;
//        startStage("/main/review.fxml", 655, 290);
    }
}
