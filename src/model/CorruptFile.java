package model;

import com.jfoenix.controls.JFXButton;
import javafx.scene.layout.HBox;

import javax.xml.soap.Node;
import java.io.File;
import java.io.IOException;

import static main.Helper.*;

public class CorruptFile {
    private File file;
    private String path;
    private String message;
    private String controls;

    public CorruptFile(File file, String message) {
        this.file = file;
        this.message = message;
    }

    public File getFile() {
        return file;
    }

    public String getMessage() {
        return message;
    }

    public HBox getControls() {
        JFXButton view = createButton("Show in File Explorer", "#eeeeee");
        JFXButton preview = createButton("Preview", "#eeeeee");
        preview.setOnAction(event -> {
            previewFile(file.getAbsolutePath());
        });
//        JFXButton view = createButton("Review", "#1976d2");
        view.setOnAction(event -> {
            try {
                openFileLocation(file.getAbsolutePath());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        HBox hBox = new HBox();
        hBox.getChildren().addAll(view, preview);
        hBox.setSpacing(10);
        return hBox;
    }

    public String getPath() {
        return file.getAbsolutePath();
    }
}
